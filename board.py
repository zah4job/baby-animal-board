import json
import keyboard

import pygame
from loguru import logger
from pygame import mixer


with open('./settings.json', 'rt') as settings_file:
    animal_keys = json.loads(settings_file.read())


def play_animal_sound(animal_name: str) -> None:
    """Load mp3 file and start playing."""
    mixer.music.unload()
    file_name = f'./music/{animal_name}.mp3'
    try:
        mixer.music.load(file_name)
    except pygame.error as exc:
        logger.error(exc)
        return
    logger.info(f'playing sound for: {animal_name}')
    mixer.music.play()


def process_key(event, *args, **kwargs):
    """Обработка нажатия на кнопку."""
    if not event.event_type == 'down':
        return
    key_value = event.name
    logger.debug(f'pressed {key_value}')
    try:
        animal_name = animal_keys[key_value]
    except KeyError:
        logger.debug(f'key {key_value} have not bindings')
    else:
        play_animal_sound(animal_name)


if __name__ == '__main__':
    logger.info('hi from board')
    mixer.init()
    logger.info('mixer ready')
    keyboard.hook(process_key)
    keyboard.wait()


