# Baby Animal Board software fo raspberry

only poetry environment required to run programm.

Ensure, that poetry is installed by command:

```shell
poetry --version
```

If it's not, than execute:

```shell
pip3 install poetry
```

and we ready to go:

```shell
poetry install
```

```shell
poetry run python board.py
```
